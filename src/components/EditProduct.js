import { Form, Button, Modal } from "react-bootstrap"

// import { ProductContext } from '../contexts/ProductContext';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import {useContext, useState, useEffect} from 'react';

import UserContext from "../UserContext"

import ProductData from "../components/ProductData";

import Swal from "sweetalert2"

export default function EditProductForm({theProduct}){


	const { updatedproduct, setUpdatedProduct } = useContext(UserContext)

    const id = theProduct._id;

	const [allProducts, setAllProducts] = useState([]);
    const [productId, setProductId] = useState(id);
    const [name, setName] = useState(theProduct.name);
    const [description, setDescription] = useState(theProduct.description);
    const [price, setPrice] = useState(theProduct.price);
    const [stocks, setStocks] = useState(theProduct.stocks);


    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);

    // const {updateProduct} = useContext(ProductContext);

    const updatedProduct = {id, name, description, price, stocks}


    const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`,
		{
			headers: {
				'Authorization': `Bearer ${localStorage.getItem("token")}`
				}
		})
		.then(res => res.json())
		.then(data => {
			setAllProducts(data.map(product => {
				console.log({product})
				return(
					<tr key={product._id}>
						<ProductData product={product} />
					</tr>
				)
			}))
		})
	}

    // onClick product function for add product
	function editProduct(e){

		//prevents the page redirection via form submit
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`, {
	    	method: "PUT",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				stocks: stocks

			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Product succesfully Updated",
	    		    icon: "success",
	    		    text: `${name} is now updated`
				});

				fetchData()
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again!."
				});
			}
		})		
	}

	useEffect(() => {
        fetchData()
    }, [])

	return(
		<>
			<Form onSubmit ={(e) => editProduct(e)}>
				<Modal.Body>
            		<Form.Group className="mb-3" controlId="name">
		              	<Form.Label>Product Name</Form.Label>
		              	<Form.Control type="text" placeholder="Input product name" value={name} onChange={e => setName(e.target.value)} autoFocus />
		              	<Form.Control type="hidden" placeholder="Input product name" value={id} onChange={e => setProductId(e.target.value)} />
		            </Form.Group>
		            <Form.Group className="mb-3" controlId="description">
		              	<Form.Label>Description</Form.Label>
		              	<Form.Control as="textarea" rows={4} value={description} onChange={e => setDescription(e.target.value)}/>
		            </Form.Group>
		            <Form.Group className="mb-3" controlId="price">
		              	<Form.Label>Price</Form.Label>
		              	<Form.Control type="number" placeholder="Input product price" value={price} onChange={e => setPrice(e.target.value)}/>
		            </Form.Group>
		            <Form.Group className="mb-3" controlId="stocks">
		              	<Form.Label>Stocks</Form.Label>
		              	<Form.Control type="number" placeholder="Input product quantity" value={stocks}  onChange={e => setStocks(e.target.value)}/>
		            </Form.Group>
	    	    </Modal.Body>
	    	    <Modal.Footer>
    	            <Button variant="success" type="submit">
	                	Update Product
	            	</Button>
	    		</Modal.Footer>
	        </Form>
		</>

	)
}