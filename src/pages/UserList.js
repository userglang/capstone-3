import { Table, Button, Form, Modal, Container, Navbar, NavbarBrand, Nav, NavItem } from 'react-bootstrap';

import { useState, useEffect, useContext } from "react";

import UserData from "../components/UserData";

import UserContext from "../UserContext"

import { Navigate, Link } from "react-router-dom";

import Swal from "sweetalert2"



export default function UserList(){

  	// useContect
	const { user } = useContext(UserContext)

	// useState
	const [userListData, setUserLisData] = useState([]);

	// "fetchData()" wherein we can invoke if their is a certain change with the course
	
	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`,
		{
			headers: {
				'Authorization': `Bearer ${localStorage.getItem("token")}`
				}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setUserLisData(data.map(userList => {
				return(
					<tr key={userList._id}>
						    
							<UserData userList={userList} />
					</tr>
				)
			}))
		})
	}




	useEffect(() => {
		fetchData();
	}, [])

	return(

		(user.isAdmin)
		?
		<>
			<Container className="pt-3">
				<Navbar >
					<Container>
						<NavbarBrand><h1 className="header-admin">Order Dashboard</h1></NavbarBrand>
						<Nav>
							<NavItem>
								<Button variant="primary" size="sm" as={Link} to={`/admin`} className="mx-2">Products</Button>
								<Button variant="success" size="sm" as={Link} to={`/order`} className="mx-2">Orders</Button>
							</NavItem>
						</Nav>
						
					</Container>
				</Navbar>
				{/*Table Start*/}
				<Table striped bordered hover>
				    <thead>
				        <tr>
				          	<th>User Id</th>
				          	<th>Customer Name</th>
				          	<th>Email</th>
				          	<th>Is Admin</th>
				          	<th>Action</th>
				        </tr>
				    </thead>
				    <tbody>
				        { userListData }
				    </tbody>
				</Table>
				{/*Table End*/}
			</Container>

		</>
		:
		<Navigate to="/products" />
	)
}