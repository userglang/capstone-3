import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";

import { Link } from "react-router-dom";
import { Navigate, useNavigate } from "react-router-dom";
import { Nav, Form, Button, Container, Card, Row, Col } from "react-bootstrap";
import Swal from "sweetalert2";

import Register from "../pages/Register";

export default function Login(){

	const {user, setUser} = useContext(UserContext);

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	//State to determine whether the button is enabled or not
	const [isActive, setIsActive] = useState(false);

	function login(e){
		//prevents the page redirection via form submit
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				// values are coming from our State hooks
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);
			console.log(typeof data.access)

			if(typeof data.access !== "undefined"){
				localStorage.setItem("token", data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to E-Shop!"
				});
			}
			else{
				Swal.fire({
					title: "Authetication Failed",
					icon: "error",
					text: "Check your login details and try again."
				});
			}

		})


		//Clear input fields
		setEmail("");
		setPassword("");


	}

	//Retrieve user details
	// we will get the payload from the token.
	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/myDetails`,{
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			//This will be set to the user state.
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() =>{
		// Validation to enable the submit button when all fields are populated.
		if(email !== "" && password !== ""){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	}, [email, password])

	return(
		// Conditional rendering is applied in the Login pages that will determine which component/page will be loaded if the user is already login.
		(user.id !== null)
		?	
			// Redirected to the /product endpoint.
			<Navigate to="/products"/>
		:
		<>
			<div className="bg-image">
				<div className="Auth-form-container">
			      <form className="Auth-form" onSubmit ={(e) => login(e)}>
			        <div className="Auth-form-content">
			          <h3 className="Auth-form-title">Sign In</h3>
			          <div className="form-group mt-3">
			            <label>Email address</label>
			            <input
			              type="email"
			              className="form-control mt-1"
			              placeholder="Enter email"
			              value={email} onChange={e => setEmail(e.target.value)}
			            />
			          </div>
			          <div className="form-group mt-3">
			            <label>Password</label>
			            <input
			              type="password"
			              className="form-control mt-1"
			              placeholder="Enter password"
			              value={password} onChange={e => setPassword(e.target.value)}
			            />
			          </div>
			          <div className="d-grid gap-2 mt-3">
			            {
							isActive
							?
								<Button variant="primary" type="submit" id="submitBtn">
								  Sign in
								</Button>
							:
								<Button variant="primary" type="submit" id="submitBtn" disabled>
								  Sign in
								</Button>
						}
			          </div>
			          <p className="forgot-password text-right mt-4">
			            <Link as={Link} to="/register">Create new account!</Link>
			          </p>
			        </div>
			      </form>
			    </div>
			</div>

		</>
	)
}