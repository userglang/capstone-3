import {useState, useEffect} from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { UserProvider } from "./UserContext";

import AppNavbar from "./components/AppNavbar";

import AdminDashboard from "./pages/AdminDashboard";
import Cart from "./pages/Cart";
import Products from "./pages/Products";
import ViewProduct from "./pages/ViewProduct";
import EditProduct from "./pages/EditProduct"
import Error from "./pages/Error";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Order from "./pages/Order";
import Register from "./pages/Register";
import UserOrder from "./pages/UserOrder";
import UserList from "./pages/UserList";

import {Container} from "react-bootstrap";
import './App.css';

function App() {

  //Global state
  //To store the user information and will be used for validation if a user is already logged in on the app or not.
  const [user, setUser] = useState({
            //null
    // email: localStorage.getItem("email")
    id: null,
    isAdmin: null
  })


    const [product, setProduct] = useState({
              //null
      // email: localStorage.getItem("email")
      id: null,
      updatedProduct: []
    })


  //we can check the changes in our User state.

  //Function for clearing localStorage on logout
  const unsetUser = () =>{
    localStorage.clear();
  }


  // To update the User State upon page load if a user already exist.
  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/users/myDetails`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {

      //set the user states values with the user details upon successful login
      if(typeof data._id !== "undefined"){
          setUser({
            //undefined
            id: data._id,
            isAdmin: data.isAdmin
          })
      }
      //set back the initial state of the user
      else{
          setUser({
              id: null,
              isAdmin: null
            })
      }
      
    })
  }, [])

  return (

    
        //Store a user state and unsetUSer function in our "UserProvider". This information will be pass through all the components and to validate if their is already a logged in user.
    <UserProvider value={{user, setUser, unsetUser, product}}>
        {/*Router component is used to wrapped around all components which will have access to our routing system.*/}
        <Router>
          <AppNavbar />
              {/*Routes holds all our Route components.*/}
              <Routes>
              {/*
                  - Route assigns an endpoint and displays the appropriate page component for that endpoint.
                  - "path" attribute assigns the endpoint.
                  - "element" attribute assigns page component to be displayed at the endpoint.
              */}
                  <Route exact path = "/" element={<Home />} />
                  <Route exact path = "/admin" element={<AdminDashboard />} />
                  <Route exact path = "/order" element={<Order />} />
                  <Route exact path = "/cart" element={<Cart />} />
                  <Route exact path = "/editProduct/:productId" element={<EditProduct />}/>
                  <Route exact path = "/order/:orderId" element={<UserOrder />}/>
                  <Route exact path = "/products" element={<Products />} />
                  <Route exact path = "/products/:productId" element={<ViewProduct />} />
                  <Route exact path = "/register" element={<Register />} />
                  <Route exact path = "/login" element={<Login />} />
                  <Route exact path = "/logout" element={<Logout />} />
                  <Route exact path = "/users" element={<UserList />} />
                  <Route exact path = "*" element={<Error />} />
              </Routes>
        </Router>
    </UserProvider>
  );
}


export default App;
